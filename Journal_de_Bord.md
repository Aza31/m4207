### Journal de bord 03/02/2017

Mon collègue Ludovic s'étant occupé de la localisation par GPS, je me charge aujourd'hui de l'alarme en cas de batterie faible du système.
Pour cela nous avons le code suivant qui permet de jouer une mélodie :

```c
int speakerPin = 4;

int length = 15; // the number of notes
char notes[] = "ccggaagffeeddc "; // a space represents a rest
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 300;

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };

  // play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}

void setup() {
  pinMode(speakerPin, OUTPUT);
}

void loop() {
  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } else {
      playNote(notes[i], beats[i] * tempo);
    }

    // pause between notes
    delay(tempo / 2); 
  }
}
```

Nous avons donc la base il nous faut maintenant le modifier pour jouer une autre sonnerie
Voici donc le programme modifié, modification du tempo et des notes a jouer :

```c
int speakerPin = 4;

int length = 15; // the number of notes
char notes[] = "fe fe fe fe fe "; // a space represents a rest
int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
int tempo = 50;

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };

  // play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}

void setup() {
  pinMode(speakerPin, OUTPUT);
}

void loop() {
  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } else {
      playNote(notes[i], beats[i] * tempo);
    }

    // pause between notes
    delay(tempo / 2); 
  }
}
```

## Problème nous n'avons que 8 notes possibles

Conséquence : Choix des mélodies limités

Début de code pour réussir à jouer toutes les musiques possibles:
## Problème la fonction tone ne fonctionne pas
```c
// NB: ALL NOTES DEFINED WITH STANDARD ENGLISH NAMES, EXCEPT FROM "A" 
//THAT IS CALLED WITH THE ITALIAN NAME "LA" BECAUSE A0,A1...ARE THE ANALOG PINS ON ARDUINO.
// (Ab IS CALLED Ab AND NOT LAb)
float  C0 = 16.35;
float Db0 = 17.32;
float D0 = 18.35;
float Eb0 = 19.45;
float E0 = 20.60;
float F0 = 21.83;
float Gb0 = 23.12;
float G0 = 24.50;
float Ab0 = 25.96;
float LA0 = 27.50;
float Bb0 = 29.14;
float B0  = 30.87;
float C1  = 32.70;
float Db1 = 34.65;
float D1  = 36.71;
float Eb1 = 38.89;
float E1 = 41.20;
float F1 = 43.65;
float Gb1 = 46.25;
float G1 = 49.00;
float Ab1 = 51.91;
float LA1 = 55.00;
float Bb1 = 58.27;
float B1 = 61.74;
float C2 = 65.41;
float Db2 = 69.30;
float D2 = 73.42;
float Eb2 = 77.78;
float E2 = 82.41;
float F2 = 87.31;
float Gb2 = 92.50;
float G2 = 98.00;
float Ab2 = 103.83;
float LA2 = 110.00;
float Bb2 = 116.54;
float B2 = 123.47;
float C3 = 130.81;
float Db3 = 138.59;
float D3 = 146.83;
float Eb3 = 155.56;
float E3 = 164.81;
float F3 = 174.61;
float Gb3 = 185.00;
float G3 = 196.00;
float Ab3 = 207.65;
float LA3 = 220.00;
float Bb3 = 233.08;
float B3 = 246.94;
float C4 = 261.63;
float Db4 = 277.18;
float D4 = 293.66;
float Eb4 = 311.13;
float E4 = 329.63;
float F4 = 349.23;
float Gb4 = 369.99;
float G4 = 392.00;
float Ab4 = 415.30;
float LA4 = 440.00;
float Bb4 = 466.16;
float B4 = 493.88;
float C5 = 523.25;
float Db5 = 554.37;
float D5 = 587.33;
float Eb5 = 622.25;
float E5 = 659.26;
float F5 = 698.46;
float Gb5 = 739.99;
float G5 = 783.99;
float Ab5 = 830.61;
float LA5 = 880.00;
float Bb5 = 932.33;
float B5 = 987.77;
float C6 = 1046.50;
float Db6 = 1108.73;
float D6 = 1174.66;
float Eb6 = 1244.51;
float E6 = 1318.51;
float F6 = 1396.91;
float Gb6 = 1479.98;
float G6 = 1567.98;
float Ab6 = 1661.22;
float LA6 = 1760.00;
float Bb6 = 1864.66;
float B6 = 1975.53;
float C7 = 2093.00;
float Db7 = 2217.46;
float D7 = 2349.32;
float Eb7 = 2489.02;
float E7 = 2637.02;
float F7 = 2793.83;
float Gb7 = 2959.96;
float G7 = 3135.96;
float Ab7 = 3322.44;
float LA7 = 3520.01;
float Bb7 = 3729.31;
float B7 = 3951.07;
float C8 = 4186.01;
float Db8 = 4434.92;
float D8 = 4698.64;
float Eb8 = 4978.03;
// DURATION OF THE NOTES 
int BPM = 120;    //  you can change this value changing all the others
int Q = 60000/BPM; //quarter 1/4 
int H = 2*Q; //half 2/4
int E = Q/2;   //eighth 1/8
int S = Q/4; // sixteenth 1/16
int W = 4*Q; // whole 4/4

void setup() {     
pinMode(4, OUTPUT);   
pinMode(5, OUTPUT);       
digitalWrite(5,LOW);

}

// the loop routine runs over and over again forever:
void loop() {
  //tone(pin, note, duration)
    tone(4,LA3,Q); 
    delay(1+Q); //delay duration should always be 1 ms more than the note in order to separate them.
    tone(4,LA3,Q);
    delay(1+Q);
    tone(4,LA3,Q);
    delay(1+Q);
    tone(4,F3,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    
    tone(4,LA3,Q);
    delay(1+Q);
    tone(4,F3,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    tone(4,LA3,H);
    delay(1+H);
    
    tone(4,E4,Q); 
    delay(1+Q); 
    tone(4,E4,Q);
    delay(1+Q);
    tone(4,E4,Q);
    delay(1+Q);
    tone(4,F4,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    
    tone(4,Ab3,Q);
    delay(1+Q);
    tone(4,F3,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    tone(4,LA3,H);
    delay(1+H);
    
    tone(4,LA4,Q);
    delay(1+Q);
    tone(4,LA3,E+S);
    delay(1+E+S);
    tone(4,LA3,S);
    delay(1+S);
    tone(4,LA4,Q);
    delay(1+Q);
    tone(4,Ab4,E+S);
    delay(1+E+S);
    tone(4,G4,S);
    delay(1+S);
    
    tone(4,Gb4,S);
    delay(1+S);
    tone(4,E4,S);
    delay(1+S);
    tone(4,F4,E);
    delay(1+E);
    delay(1+E);//PAUSE
    tone(4,Bb3,E);
    delay(1+E);
    tone(4,Eb4,Q);
    delay(1+Q);
    tone(4,D4,E+S);
    delay(1+E+S);
    tone(4,Db4,S);
    delay(1+S);
    
    tone(4,C4,S);
    delay(1+S);
    tone(4,B3,S);
    delay(1+S);
    tone(4,C4,E);
    delay(1+E);
    delay(1+E);//PAUSE QUASI FINE RIGA
    tone(4,F3,E);
    delay(1+E);
    tone(4,Ab3,Q);
    delay(1+Q);
    tone(4,F3,E+S);
    delay(1+E+S);
    tone(4,LA3,S);
    delay(1+S);
    
    tone(4,C4,Q);
    delay(1+Q);
    tone(4,LA3,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    tone(4,E4,H);
    delay(1+H);
    
    tone(4,LA4,Q);
    delay(1+Q);
    tone(4,LA3,E+S);
    delay(1+E+S);
    tone(4,LA3,S);
    delay(1+S);
    tone(4,LA4,Q);
    delay(1+Q);
    tone(4,Ab4,E+S);
    delay(1+E+S);
    tone(4,G4,S);
    delay(1+S);
    
    tone(4,Gb4,S);
    delay(1+S);
    tone(4,E4,S);
    delay(1+S);
    tone(4,F4,E);
    delay(1+E);
    delay(1+E);//PAUSE
    tone(4,Bb3,E);
    delay(1+E);
    tone(4,Eb4,Q);
    delay(1+Q);
    tone(4,D4,E+S);
    delay(1+E+S);
    tone(4,Db4,S);
    delay(1+S);
    
    tone(4,C4,S);
    delay(1+S);
    tone(4,B3,S);
    delay(1+S);
    tone(4,C4,E);
    delay(1+E);
    delay(1+E);//PAUSE QUASI FINE RIGA
    tone(4,F3,E);
    delay(1+E);
    tone(4,Ab3,Q);
    delay(1+Q);
    tone(4,F3,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    
    tone(4,LA3,Q);
    delay(1+Q);
    tone(4,F3,E+S);
    delay(1+E+S);
    tone(4,C4,S);
    delay(1+S);
    tone(4,LA3,H);
    delay(1+H);
    
    delay(2*H);
    
}
```
Pour la prochaine séance il faudra coder la connexion à la WiFi pour l'envoi des données GPS